from django.urls import path
from . views import *


app_name = "tayeur"
urlpatterns =[
    path("login/", loginView, name="login"),
    path("", client, name="client"),
    path("commande/", commande, name="commande"),
    path("accueil",accueil, name="accueil"),
   
]


from django.db import models
from django.urls import reverse

class Client(models.Model):
    name = models.CharField(max_length=200)
    tel = models.CharField(max_length=250)
    pays = models.CharField(max_length=250)
    ville = models.CharField(max_length=250)
    adress = models.CharField(max_length=250)
    epaule = models.CharField(max_length=250, default=0)
    manche = models.CharField(max_length=250, default=0)
    coude =  models.CharField(max_length=250, default=0)
    poitrine =  models.CharField(max_length=250, default=0)
    taille =  models.CharField(max_length=250, default=0)
    ceinture =  models.CharField(max_length=250, default=0)
    

    def get_absolute_url(self):
        return reverse('author-detail', kwargs={'pk': self.pk})


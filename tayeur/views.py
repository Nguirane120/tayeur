from django.shortcuts import render, redirect, get_object_or_404
from django.views import generic
from django.views.generic.edit import CreateView
from .models import Client

from .forms import Login
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login

def loginView(request):
    form = Login(request.POST or None)
    if form.is_valid():
        #verification du formulaire
        username = form.cleaned_data["username"]
        password = form.cleaned_data["password"]
        #authentication du user
        user = authenticate(username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect("tayeur:accueil")

    return render(request, "tayeur/login.html", {"form":form})

def accueil(request):
    return render(request, "tayeur/accueil.html")

def client(request):
    client = Client()
    return render(request, "tayeur/client-template.html", {"client":client})


def commande(request):
    return render(request, "tayeur/commande-template.html")